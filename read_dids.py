
# json
import json
# regex expressions
import re
import os.path
# folder utility
from os import path
# UI
# from tkinter import *
# cmd line
import sys

#  ASSUMPTIONS - these should come from json config file
DID_NUMBER_APPEARANCES_POSITIVEREPONSE = 3
DID_NUMBER_APPEARANCES_NEGATIVEREPONSE = 1
POSITIVERESPONSE_PATTERN = "Positive Response"
NEGATIVERESPONSE_PATTERN = "Negative Response"

# logfile
# LogFile_name = "ab"
# LogFile_name = '2019-12-19_141012_DETMainLog_ADD.rtf'
# LogFile_name = '2019-12-19_141012_DETMainLog_ADD_0x207E_x5.rtf'

# JSON configuration file
JSON_config_file = 'AddSpec_ALL.json'
# JSON_config_file = 'AddSpec_0x207E.json'
# JSON_config_file = 'AddSpec_0x207F.json'
JSON_config_file = 'AddSpec_0x2080.json'

# DID_numbers = ["0x207C", "0x207D", "0x207E", "0x207F", "0x2080", "0x2081", "0x2082", "0x2083", "0x2084", "0x2085", "0x2011"]  # all Responses

# DID_numbers = ["0x207C", "0x207E", "0x2081", "0x2082", "0x2083", "0x207D", "0x207F", "0x2085", "0x2011"]  # Positive Responses
# DID_numbers = ["0x207E"]  # Multiple Responses0


# DID_numbers = ["0x207C", "0x2080", "0x2081", "0x2111"]  # 0x2011 non existent DID Response
# DID_numbers = ["0x2080"]                # Negative Response
# DID_numbers = ["0x2080", "0x2084", "0x2011"]      # Negative Response
# DID_numbers = ["0x207C", "0x207D", "0x207F", "0x2080", "0x2081", "0x2082", "0x2083", "0x2084", "0x2085"]
# DID_numbers = ["0x207D", "0x207C", "0x207F", "0x2081", "0x2082", "0x2083", "0x2085", "0x207E", "0x2011"]
# DID_numbers = ["0x207D", "0x207C", "0x207F", "0x2081", "0x2082", "0x2083", "0x2085"]
# DID_numbers = ["0x207D", "0x207C", "0x207E"]
# DID_numbers = ["0x2080"]    # Negative Response
# 0x207C - match ok,   406  bytes
# 0x207E - match ok,    70  bytes   1 Positive Response, 1 Negative Response
# 0x2081 - match ok,    78  bytes
# 0x2082 - match ok,    52  bytes
# 0x2083 - match ok,   354  bytes
# 0x207D - match OK,   152  bytes
# 0x207F - match NOK,  270  bytes
# 0x2085 - match NOK,  358  bytes
# 0x2080 - match NOK,   CNC in logfile
# 0x2084 - match NOK,   CNC in logfile


# get the list of all DIDs found in the JSON config file
def get_DID_numbers():
    DIDs_found = []
    with open(JSON_config_file, "r") as config_file:
        # load the JSON configuration file as JSON object
        config = json.load(config_file)

    # we have a list with all DIDs found in the json config file
    DID_list: list = config["DataIdentifiers"]
    # print()
    # print(DID)

    for DID in DID_list:
        DID_HexNr: str = DID['DID'][1]["HexNr"]
        DIDs_found.append(DID_HexNr)
        # print(DID_HexNr)

    # print(DIDs_found)
    return DIDs_found


def print_usage():
    print("\n\nread_dids usage:\n")
    print("read_dids /?  -  prints this message\n")
    print("read_dids     -  without a parameter => the GUI will start\n")
    print("read_dids logfile.rtf  -  use this file as input")
    print("                          the output will be found in /out folder\n\n")
    print("the most common usage: read_dids ADD_Test_2020.rtf\n")


def check_arguments():
    number_of_arguments = len(sys.argv) - 1
    FileName_as_Parameter: int = -1
    FileName: str = ""

    # check for '/?'
    if 1 == number_of_arguments:
        if "/?" == sys.argv[1]:
            print_usage()
        else:
            FileName = sys.argv[1]
            if not path.exists(FileName):
                print('\nfile "{0}" does not exist\n'.format(FileName))
                print("use a filename for an existent file")
            else:
                FileName_as_Parameter = 1

    if 0 == number_of_arguments:
        # run the GUI
        FileName_as_Parameter = 0

    if 1 < number_of_arguments:
        print_usage()

    return FileName_as_Parameter, FileName


# main process function
# - called from main/root
# - it will call all the sub-functions to process the input/logfile
def process_input():
    DID_list = get_DID_numbers()

    for DID_number in DID_list:
        if 1 == 1:
            if not path.exists('out\\\\'):
                os.mkdir('out\\\\')

            DID_output: str = "out\\out_{0}.txt".format(DID_number)

            file_DID_output = open(DID_output, "w")

            with open(LogFile_name, "r") as file_R:
                content: str = file_R.read().replace('\n', '')  # content of the log file

            (found_search, found_findall) = (re.search(str(DID_number), content), re.findall(str(DID_number), content))
            DID_found_number_of_times: int = len(re.findall(DID_number, content))

            offset = 0
            DID_instances_found_to_be_processed: int = DID_found_number_of_times

            # as DID Responses are not the same as DID instances, we need to compute them as well
            DID_Responses = 0  # Negative, Positive

            while DID_instances_found_to_be_processed > 0:
                # doing the loop for the current appearance of DID in the log - could be PositiveResponse or a NegativeResponse
                CurrentResponse_chunks: list = re.split(str(DID_number), content)
                CurrentResponse_chunk_1 = CurrentResponse_chunks[1 + offset]

                # check if PositiveResponse or NegativeResponse
                PosRes_Found = re.search(POSITIVERESPONSE_PATTERN, CurrentResponse_chunk_1)
                NegRes_Found = re.search(NEGATIVERESPONSE_PATTERN, CurrentResponse_chunk_1)
                file_DID_output.write(10 * "* * " + "\n")
                file_DID_output.write("DID appearance   {0}\n".format(str(DID_Responses + 1)))
                (PosRes, NegRes) = (hasattr(PosRes_Found, "span"), hasattr(NegRes_Found, "span"))

                # checking for which kind of Response is the first one: Positive or Negative
                if (PosRes and NegRes is False) or \
                        ((PosRes is True and NegRes is True) and (PosRes_Found.span() < NegRes_Found.span())):
                    # we have a Positive Response
                    # we get the bytes of Response as payload
                    CurrentResponse_chunk_2 = CurrentResponse_chunks[2 + offset]

                    # formatting each chunk we found - stripping unneeded characters
                    CurrentResponse_chunk_1 = CurrentResponse_chunk_1.replace("\\cf4", '').replace("\\par", '').strip()
                    CurrentResponse_chunk_1 = re.split("DataIdentifier", CurrentResponse_chunk_1)[0].strip()
                    CurrentResponse_chunk_2 = re.split(':', CurrentResponse_chunk_2)[2].strip().split('[')[0].strip("")
                    CurrentResponse_chunk_2 = CurrentResponse_chunk_2.replace("\\par", '').strip()

                    # we count the number of bytes we found in the DID response
                    # we are looking for "00 " format, eg. "00 00 00" counting as 3 bytes
                    bytes_number = len(re.findall("([0-9a-fA-F]+ *)", CurrentResponse_chunk_2))

                    # save information to file
                    file_DID_output.write(10 * "* * " + "\n")
                    file_DID_output.write("DID number: {0} : Found as {1}\n".format(str(DID_number), POSITIVERESPONSE_PATTERN))
                    file_DID_output.write("number of bytes found in the PositiveResponse: {0} bytes\n".format(str(bytes_number)))
                    file_DID_output.write(10 * "= = " + "\n")

                    # print the chunks of the DID we found in the log
                    file_DID_output.write("Header:\n" + CurrentResponse_chunk_1 + "\n")
                    file_DID_output.write(10 * "= = " + "\n")
                    file_DID_output.write("Payload:\n" + did_decode(CurrentResponse_chunk_2, DID_number) + "\n")

                    file_DID_output.write(10 * "* * " + "\n\n")

                    #  prepare for the next section / step
                    DID_instances_found_to_be_processed = DID_instances_found_to_be_processed - DID_NUMBER_APPEARANCES_POSITIVEREPONSE
                    offset = offset + DID_NUMBER_APPEARANCES_POSITIVEREPONSE
                    DID_Responses = DID_Responses + 1
                else:
                    # we have a Negative Response
                    # noinspection PyPep8
                    NegativeResponse_chunks = re.split("\\\par", CurrentResponse_chunk_1)

                    NegativeResponse_chunk_1 = NegativeResponse_chunks[1].strip("\\cf3").strip()
                    NegativeResponse_chunk_2 = NegativeResponse_chunks[2].strip("\\cf3").strip()

                    # save information to file
                    file_DID_output.write(10 * "* * " + "\n")
                    file_DID_output.write("DID number: {0} : Found as {1}\n ".format(str(DID_number), NEGATIVERESPONSE_PATTERN))
                    file_DID_output.write(10 * "= = " + "\n")
                    file_DID_output.write(NegativeResponse_chunk_1 + "\n")
                    file_DID_output.write(10 * "= = " + "\n")
                    file_DID_output.write(NegativeResponse_chunk_2 + "\n")
                    file_DID_output.write(10 * "* * " + "\n\n")

                    #  prepare for the next section / step
                    DID_instances_found_to_be_processed = DID_instances_found_to_be_processed - DID_NUMBER_APPEARANCES_NEGATIVEREPONSE
                    offset = offset + DID_NUMBER_APPEARANCES_NEGATIVEREPONSE
                    DID_Responses = DID_Responses + 1

                if 0 == DID_found_number_of_times:
                    break

            print('DID_number {0} DID_Responses {1}'.format(str(DID_number), str(DID_Responses)))
            file_DID_output.close()
            line_2 = 10 * "++++" + "\n"
            line_1 = "DID {0} was found: {1} times".format(str(DID_number), str(DID_Responses))
            line_prepender(DID_output, line_2)
            line_prepender(DID_output, line_1)
            line_prepender(DID_output, line_2)


# utility function - prepend one line at the file beginning
def line_prepender(filename: str, line: str):
    with open(filename, 'r+') as f:
        file_content: str = f.read()
        f.seek(0, 0)
        f.write(line.rstrip('\r\n') + '\n' + file_content)


# utility function - convert hex-bytes ( 'AA' ) to integers
def hex_2_int(hex_str: str):
    nibble = "0x{0}".format(hex_str)
    dec = int(nibble, 16)
    return dec


# utility function - convert ODO ( counter + unit ) to distance ( km )
def convert_ODO_to_distance(odo_hex: int, resolution: float):
    distance: float = hex_2_int(odo_hex) * resolution
    distance = "{0} km".format(str(distance))
    return distance


# utility function - add '0x' before a hex-byte
def prepend_0x(hex_str: str):
    return "0x{0}".format(hex_str)


def compile_message(incoming: str, line: str) -> str:
    return "{0}\n{1}".format(incoming, line)


# decode the string coming as a payload into corresponding fields/ subfields
def did_decode(payload: str, DID_to_be_decoded: str) -> str:
    with open(JSON_config_file, "r") as config_file:
        # load the JSON configuration file as JSON object
        config = json.load(config_file)

    # we strip away the " " spaces between bytes
    payload = payload.replace(" ", "")

    # print("DID_to_be_decoded: " + DID_to_be_decoded)
    DID_list: list = config["DataIdentifiers"]
    # print(DID_list)
    # print()
    for DID in DID_list:
        DID_Name: str = DID['DID'][0]["Name"]
        # print(DID)
        # print("DID_Name: " + DID_Name)
        DID_HexNr: str = DID['DID'][1]["HexNr"]  # DataIdentifiers   DID     HexNr
        # print("DID_HexNr: " + DID_HexNr)
        if DID_HexNr == DID_to_be_decoded:
            DID_Name:       str  = DID['DID'][0]["Name"]
            DID_Type:       str  = DID['DID'][2]["Type"]       # DataIdentifiers   DID     Type
            DID_ByteSize:   int  = DID['DID'][3]["ByteSize"]   # DataIdentifiers   DID     ByteSize
            # print("Name {0}, HexNr {1}, Type {2}, ByteSize {3} ".format(DID_Name, DID_HexNr, DID_Type, str(DID_ByteSize)))

            DID_Decoded: str = ""
            DID_Decoded = compile_message(DID_Decoded, "Name      : {0}".format(DID_Name))
            DID_Decoded = compile_message(DID_Decoded, "HexNr     : {0}".format(DID_HexNr))
            DID_Decoded = compile_message(DID_Decoded, "Type      : {0}".format(DID_Type))
            DID_Decoded = compile_message(DID_Decoded, "ByteSize  : {0}".format(DID_ByteSize))

            DID_SubField_0: list = DID['DID'][5]["SubFields"][0]["SubField"]  # SubField    Version
            (start, length) = (2 * DID_SubField_0[2]["StartByte"], 2 * DID_SubField_0[3]["Length"])
            # we multiply by 2, because 1 byte has 2 characters ( eg. 'A5' or 'FF' )
            version = payload[start: start + length]
            DID_Decoded = compile_message(DID_Decoded, "Version   : {0}".format(str(version)))

            DsSubFields: list = DID['DID'][4]["DsSubFields"]  # list of DS-es
            Number_of_DSes: int = len(DsSubFields)
            # DS_0 = DsSubFields[0]["DsSubField"]       # DS900 or DS i
            # DS_1 = DsSubFields[1]["DsSubField"]       # DS901 or DS i+1

            # iterate over each DS found in JSON
            for i in range(0, Number_of_DSes):
                DS: list = DsSubFields[i]["DsSubField"]
                (DsId, DsDscr, DsStartByte, DsLength, DsFields) = (
                    DS[0]["DsId"], DS[1]["DsDscr"], DS[2]["DsStartByte"], DS[3]["DsLength"], DS[4]["DsFields"])

                # stamp DS name and description
                DID_Decoded = compile_message(DID_Decoded, "\nDS: {0} : {1}".format(DsId, DsDscr))

                assert isinstance(DsFields, list)
                number_of_fields: int = len(DsFields)

                # iterate over each Field of current DS
                for j in range(0, number_of_fields):
                    field: list = DsFields[j]["DsField"]
                    field_name:  str = field[0]["Name"]
                    field_descr: str = field[1]["Descr"]
                    field_type:  str = field[2]["Type"]
                    field_msb:   str = field[3]["MSB"]
                    field_lsb:   str = field[4]["LSB"]
                    field_unit:  str = field[5]["Unit"]

                    # only if ODO - then we need the resolution
                    if "ODO" == field_type:
                        field_resolution: str = field[6]["Resolution"]

                    (start, stop) = (2 * field_msb, 2 * (field_lsb + 1))

                    # check if the found Field is "StateChangeCtr" type
                    # if field_name == "StateChangeCtr":
                    if re.search("StateChangeCtr_", field_name):
                        # extracting Class StateChangeCtr
                        ctr = str(payload[start: stop])
                        ctr = prepend_0x(ctr)
                        DID_Decoded = compile_message(DID_Decoded, "{0} :{1}\n".format(field_name, ctr))

                    if "ODO" == field_type:
                        (odo, odo_unit, resolution) = (payload[start: stop], field_unit, field_resolution)
                        # DID_Decoded = compile_message(DID_Decoded, "{0} : {1} : {2} {3}".format(field_name, field_descr, odo, convert_ODO_to_distance(odo, odo_unit)))
                        DID_Decoded = compile_message(DID_Decoded, "{0} : {1} : {2}".format(field_name, field_descr, convert_ODO_to_distance(odo, resolution), odo_unit))

                    if re.search("Event_[0-9]_ClassID", field_name):
                        counter_value = payload[start: stop]
                        DID_Decoded = compile_message(DID_Decoded, "{0} : {1} : {2}".format(field_name, field_descr, counter_value))

                    if re.search("Event [0-9a-fA-F]+ StateChange", field_name):
                        event_ctr = str(payload[start: stop])
                        event_ctr = prepend_0x(event_ctr)
                        DID_Decoded = compile_message(DID_Decoded, "{0} : {1} : {2}".format(field_name, field_descr, event_ctr))

                    if re.search("Event [0-9a-fA-F]+ GlobalRealTime", field_name):
                        grt = payload[start: stop]
                        # DID_Decoded = compile_message(DID_Decoded, "{0} : {1} : {2} {3} {4}".format(field_name, field_descr, grt, hex_2_int(grt), field_unit))
                        DID_Decoded = compile_message(DID_Decoded, "{0} : {1} : {2} {3}".format(field_name, field_descr, hex_2_int(grt), field_unit))

            # if len == 1 - we have only "Version Number" as parameter
            # if len == 2 - we have      "Unallocated" also
            if 2 == len(DID['DID'][5]["SubFields"]):
                DID_SubField_1: list = DID['DID'][5]["SubFields"][1]["SubField"]  # SubField    Unallocated
                (start, length) = (2 * DID_SubField_1[2]["StartByte"], 2 * DID_SubField_1[3]["Length"])
                # we multiply by 2, because 1 byte has 2 characters ( eg. 'A5' or 'FF' )
                # extract the unallocated bytes from payload and stamp it in the output txt file
                unallocated_bytes = payload[start: start + length]
                DID_Decoded = compile_message(DID_Decoded, "\nUnallocated    :\n{0}".format(str(unallocated_bytes)))

    return "payload decoded = \n{0}".format(DID_Decoded)


(FileName_from_cmdline, file_name) = check_arguments()

if 1 == FileName_from_cmdline:
    LogFile_name = file_name
    print("LogFile_name = " + LogFile_name)
    # main process function
    process_input()

if 0 == FileName_from_cmdline:
    x = 1
    print("\nstart the UI\n")

    # here - somewhere those function must be called again
    # main process function
    # process_input()




